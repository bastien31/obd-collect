package net.lamesnie.sfusilie.obdcollect;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sfusilie on 01/09/16.
 */

public class CollectData {
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    private LocationManager mLM = null;

    private Map<String, ObdData> mObdDatas = null;
    public String vin = "";
    private Location mLatestGpsLoc = null;
    private Location mLatestNetworkLoc = null;

    private final LocationListener mGpsLocationListener =new LocationListener(){

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            mLatestGpsLoc = location;
        }
    };

    private final LocationListener mNetworkLocationListener =new LocationListener(){

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            mLatestNetworkLoc = location;
        }
    };


    public void init(Activity activity){
        boolean gpsEnabled = false;
        boolean networkEnabled = false;

        requestPermissions(activity);
        mLM = (LocationManager)activity.getSystemService(Context.LOCATION_SERVICE);
        try{
            gpsEnabled = mLM.isProviderEnabled(LocationManager.GPS_PROVIDER);

        }catch(Exception exception){
            exception.printStackTrace();
        }
        try{
            networkEnabled = mLM.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception exception){
            exception.printStackTrace();
        }

        try {
            if (gpsEnabled)
                mLM.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mGpsLocationListener);
        } catch (SecurityException exception) {
            exception.printStackTrace();
        }
        try {
            if (networkEnabled)
                mLM.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mNetworkLocationListener);
        } catch (SecurityException exception) {
            exception.printStackTrace();
        }
    }

    private void requestPermissions(Activity activity){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            }
        }
    }

    public Map<String, Object> getLocation(){
        Location lastLocation = null;
        Map<String, Object> data = new HashMap<>();

        if (mLatestGpsLoc != null && mLatestNetworkLoc != null) {
            if (mLatestGpsLoc.getTime() > mLatestNetworkLoc.getTime())
                lastLocation = mLatestGpsLoc;
            else
                lastLocation = mLatestNetworkLoc;
        } else if (mLatestGpsLoc != null)
            lastLocation = mLatestGpsLoc;
        else if (mLatestNetworkLoc != null)
            lastLocation = mLatestNetworkLoc;

        if (lastLocation == null){
            if (mLM != null) {
                Location netLoc = null, gpsLoc = null;
                try {
                    gpsLoc = mLM.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                } catch (SecurityException exception) {
                    exception.printStackTrace();
                }
                try {
                    netLoc = mLM.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                } catch (SecurityException exception) {
                    exception.printStackTrace();
                }

                if (gpsLoc != null && netLoc != null) {
                    if (gpsLoc.getTime() > netLoc.getTime())
                       lastLocation = gpsLoc;
                    else
                        lastLocation = netLoc;
                } else if (gpsLoc != null)
                    lastLocation = gpsLoc;
                else if (netLoc != null)
                            lastLocation = netLoc;
            }
        }

        if (lastLocation != null) {
            Map<String, Object> geoPoint = new HashMap<>();
            geoPoint.put("type", "Point");
            ArrayList<Double> loc = new ArrayList<>();
            loc.add(lastLocation.getLongitude());
            loc.add(lastLocation.getLatitude());
            geoPoint.put("coordinates", loc);
            data.put("geoPoint", geoPoint);
            data.put("altitude", lastLocation.getAltitude());
            data.put("speed", Math.round(lastLocation.getSpeed()*3.6));
        }

        return data;
    }

    public String getTime(){
        Date date = GregorianCalendar.getInstance().getTime();
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    public void setObdData(Map<String, ObdData> data){
        mObdDatas = data;
    }

    public String getData(){
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> dataLocation;
        String result = "";

        if (mObdDatas != null) {
            dataLocation = getLocation();
            data.put("vin", vin);
            ObdData speed = mObdDatas.get("speed");
            if ((speed != null) && (Arrays.asList("0", "n/a").contains(speed.value))){
                    speed.value = String.valueOf(dataLocation.get("speed"));
            }
            dataLocation.remove("speed");
            data.put("obdData", mObdDatas);
            data.put("location", dataLocation);
            data.put("time", getTime());

            result = new Gson().toJson(data);
        }

        return result;
    }
}
