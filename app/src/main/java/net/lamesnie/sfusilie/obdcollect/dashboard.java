package net.lamesnie.sfusilie.obdcollect;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class dashboard extends AppCompatActivity {
    private String TAG = "ObdCollect";
    private WebView myWebView;
    private EditText mEdit;
    private TextView mObdDeviceId;
    private TextView mSpeedValue;
    private Button mButton;
    private ObdPush mObdPush = new ObdPush();
    private ObdDevice mObdDevice = new ObdDevice();
    private CollectData mData = new CollectData();
    private String mVID = "";
    private Boolean mObdCollectStarted = false;
    private SharedPreferences mSharedPref;

    Handler mTimerHandler = new Handler();
    Runnable obdRunnable = new Runnable() {
        @Override
        public void run() {
            Map<String, ObdData> obdDatas = new HashMap<String, ObdData>() ;
            obdDatas.put("speed", mObdDevice.getSpeed());
            mData.setObdData(obdDatas);

            mObdPush.publish(mVID, mData.getData());
            Log.i(TAG, "publish data for " + mVID + ": " + mData.getData());
            ObdData speed = obdDatas.get("speed");
            if (speed != null) {
                mSpeedValue.setText(speed.value + " " + speed.unit);
            }
            mTimerHandler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mObdDeviceId = (TextView) findViewById(R.id.obdDeviceId);
        mSpeedValue = (TextView) findViewById(R.id.speedValue);
        mButton = (Button)findViewById(R.id.btnObdCollect);
        mButton.setEnabled(false); // set button disable initially

        mSharedPref = this.getSharedPreferences(this.TAG, this.MODE_PRIVATE);


        mEdit = (EditText)findViewById(R.id.editText);
        mEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (s.toString().equals("")) {
                    mButton.setEnabled(false);
                } else {
                    mButton.setEnabled(true);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                SharedPreferences.Editor editor = mSharedPref.edit();
                editor.putString("vin", s.toString());
                editor.commit();
            }
        });

        String vin = mSharedPref.getString("vin", null);
        if (vin != null) {
            mEdit.setText(vin);
        }

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setBackgroundColor(Color.TRANSPARENT);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mObdDevice.getDeviceAddress(this);
        mObdDevice.connect();
        mObdDevice.init();
        mObdDeviceId.setText("OBD id: " + mObdDevice.GetDeviceId());
    }

    public void obdCollect(View view) {
        if (!mObdCollectStarted) {
            Log.i(TAG, "connect to MQTT server...");
            mObdPush.connect();
            Log.i(TAG, "start collecting OBD data...");
            mButton.setText("stop");

            mData.init(this);
            mVID = mObdDevice.GetVIN();
            if (mEdit != null) {
                if (mVID.equals("unknown")) {
                    mVID = mEdit.getText().toString();
                } else {
                    mEdit.setText(mVID);
                }
            }

            mData.vin = mVID;
            mTimerHandler.postDelayed(obdRunnable, 0);
            mObdCollectStarted = true;
            mEdit.setEnabled(false);

            myWebView.loadUrl("http://vps311898.ovh.net:8080/?vin=" + mVID);

        } else {
            Log.i(TAG, "stop collecting OBD data...");
            mButton.setText("start");
            mTimerHandler.removeCallbacks(obdRunnable);
            Log.i(TAG, "disconnect from MQTT server...");
            mObdPush.disconnect();
            mObdCollectStarted = false;
            mEdit.setEnabled(true);
        }
    }
}
