package net.lamesnie.sfusilie.obdcollect;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.pires.obd.commands.ObdCommand;
import com.github.pires.obd.commands.SpeedCommand;
import com.github.pires.obd.commands.control.VinCommand;
import com.github.pires.obd.commands.engine.RPMCommand;
import com.github.pires.obd.commands.protocol.EchoOffCommand;
import com.github.pires.obd.commands.protocol.LineFeedOffCommand;
import com.github.pires.obd.commands.protocol.SelectProtocolCommand;
import com.github.pires.obd.commands.protocol.TimeoutCommand;
import com.github.pires.obd.enums.ObdProtocols;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * Created by sfusilie on 31/08/16.
 */
class ObdData {
    public String value = "";
    public String unit;

    @Override
    public String toString() {
        return "" + value + ":" + unit;
    }

}


public class ObdDevice {
    public static final String BT_SERIAL = "00001101-0000-1000-8000-00805F9B34FB";
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    BluetoothSocket mSocket;
    private String mDeviceAddress = "AA:BB:CC:11:22:33";

    public void getDeviceAddress(final Object activityContext) {
        final ArrayList deviceAddrs = new ArrayList();
        final ArrayList devices = new ArrayList();
        Set pairedDevices = mBluetoothAdapter.getBondedDevices();

        for (Object device : pairedDevices) {
            BluetoothDevice btDevice = (BluetoothDevice) device;
            deviceAddrs.add(btDevice.getName() + "\n" + btDevice.getAddress());
            devices.add(btDevice.getAddress());
        }
        deviceAddrs.add("Fake data");
        devices.add("fake");

        final AlertDialog.Builder choiceDialog = new AlertDialog.Builder((Context) activityContext);

        ArrayAdapter adapter = new ArrayAdapter((Context) activityContext,
                android.R.layout.select_dialog_singlechoice,
                deviceAddrs.toArray(new String[deviceAddrs.size()]));

        choiceDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int position = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                mDeviceAddress = (String) devices.get(position);
                Log.d("ObdCollect", "Picked: " + mDeviceAddress);
            }
        });

        choiceDialog.setTitle("Choose Bluetooth device");
        choiceDialog.show();
    }

    public void connect() {
        if ((mDeviceAddress != "") && (mDeviceAddress != "fake")) {
            Log.d("ObdCollect", "Connecting to: " + mDeviceAddress);
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);

            try {
                mSocket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString(BT_SERIAL));
                mSocket.connect();
                Log.d("ObdCollect", "Connected");

            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void disconnect() {
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void init() {
        if (mSocket != null) {
            try {
//                new EchoOffCommand().run(mSocket.getInputStream(), mSocket.getOutputStream());
//                new LineFeedOffCommand().run(mSocket.getInputStream(), mSocket.getOutputStream());
//                new TimeoutCommand(5).run(mSocket.getInputStream(), mSocket.getOutputStream());
                new SelectProtocolCommand(ObdProtocols.AUTO).run(mSocket.getInputStream(),
                        mSocket.getOutputStream());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    private ObdData runCommand(ObdCommand command, String defaultValue, String defaultUnit) {
        ObdData result = new ObdData();
        if (mSocket != null) {
            try {
                command.run(mSocket.getInputStream(), mSocket.getOutputStream());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            result.value = command.getCalculatedResult();
            result.unit = command.getResultUnit();
        } else {
            Log.d("ObdCollect", "No connection to the device, returned default value.");
            result.value = defaultValue;
            result.unit = defaultUnit;
        }
        return result;
    }

    public ObdData getRpm() {
        class MyRPMCommand extends RPMCommand {
            private int myRpm = -1;

            @Override
            protected void performCalculations() {
                // ignore first two bytes [41 0C] of the response((A*256)+B)/4
                myRpm = (buffer.get(4) * 256 + buffer.get(5)) / 4;
            }

            @Override
            public String getFormattedResult() {
                return String.format("%d%s", myRpm, getResultUnit());
            }

            @Override
            public String getCalculatedResult() {
                return String.valueOf(myRpm);
            }

            @Override
            public int getRPM() {
                return myRpm;
            }

        }
        return this.runCommand(new RPMCommand(),
                String.valueOf(new Random().nextInt(4000)),
                "RPM");
    }

    public ObdData getSpeed() {
        class MySpeedCommand extends SpeedCommand {
            private int myMetricSpeed = 0;

            @Override
            protected void performCalculations() {
                // Ignore first two bytes [hh hh] of the response.
                myMetricSpeed = buffer.get(4);
            }

            @Override
            public int getMetricSpeed() {
                return myMetricSpeed;
            }

            @Override
            public float getImperialUnit() {
                return myMetricSpeed * 0.621371192F;
            }
        }
        return this.runCommand(new MySpeedCommand(),
                String.valueOf("n/a"),
                "km/h");
    }

    public String GetVIN() {
        String vin = "unknown";

        if (mSocket != null) {
            VinCommand cmd = new VinCommand();
            try {
                cmd.run(mSocket.getInputStream(), mSocket.getOutputStream());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            String result = cmd.getFormattedResult();
            if (!result.equals("")){
                vin = result;
            }
        }
        return vin ;
    }

    public String GetDeviceId(){
        String deviceId = "fake";
        if (mSocket != null) {
            deviceId = mDeviceAddress;
        }
        return deviceId;
    }
}
