package net.lamesnie.sfusilie.obdcollect;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * Created by sfusilie on 31/08/16.
 */
public class ObdPush {
    private String mServerAddr = "tcp://vps311898.ovh.net:1883";
    private String mClientId = "AndroidObdCollect";
    private int mQos = 2;
    private MemoryPersistence mPersistence = new MemoryPersistence();
    private MqttClient mMqttClient;
    private MqttConnectOptions mConnOpts;

    private void handleException(MqttException exception) {
        Log.d("ObdCollect", "reason:\t" + exception.getReasonCode());
        Log.d("ObdCollect", "msg:\t" + exception.getMessage());
        Log.d("ObdCollect", "loc:\t" + exception.getLocalizedMessage());
        Log.d("ObdCollect", "cause:\t" + exception.getCause());
        Log.d("ObdCollect", "excep:\t" + exception);
        exception.printStackTrace();
    }

    public void connect() {
        try {
            mMqttClient = new MqttClient(mServerAddr, mClientId, mPersistence);
            mConnOpts = new MqttConnectOptions();
            mConnOpts.setCleanSession(true);
            mConnOpts.setKeepAliveInterval(300);


            Log.d("ObdCollect", "Connecting to broker: " + mServerAddr);
            mMqttClient.connect(mConnOpts);
            Log.d("ObdCollect", "Connected");
        } catch (MqttException exception) {
            handleException(exception);
        }
    }

    public void disconnect() {
        if (mMqttClient != null) {
            try {
                mMqttClient.disconnect();
            } catch (MqttException exception) {
                handleException(exception);
            }
        }

    }

    public void publish(String topic, String data) {
        if (mMqttClient != null) {
            MqttMessage message = new MqttMessage(data.getBytes());
            message.setQos(mQos);

            try {
                Log.d("ObdCollect", "Sending to broker: " + topic + ":" + data);
                mMqttClient.publish(topic, message);
                Log.d("ObdCollect", "Sent");
            } catch (MqttException exception) {
                handleException(exception);
            }
        }
    }
}
